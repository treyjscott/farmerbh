% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/bet_hedging_simulation.R
\name{simulate_longterm_success}
\alias{simulate_longterm_success}
\title{Simulates long-term success for a given number of generations, patches, and fitness function}
\usage{
simulate_longterm_success(
  gens = 100,
  patches = 2,
  seed = 0.25,
  fit.fun = function(x) {     return(x/(2 * 10^5)) },
  germ_prob = 0.5,
  reps = 1,
  average = FALSE,
  costs = seq(0, 1, 0.1),
  prob_scarcities = seq(0, 1, 0.1)
)
}
\arguments{
\item{gens}{number of generations}

\item{patches}{number of soil patches}

\item{seed}{proportion of occupied patches at the start of simulations}

\item{fit.fun}{fitness function}

\item{germ_prob}{probability that spores are eliminated (from not germinating, making it to fruiting, ect)}

\item{reps}{number of replicates to simulate}

\item{average}{boolean to calculate average spore production}

\item{costs}{vector of costs to simulate}

\item{prob_scarcities}{vector of probabilities to simulate}
}
\value{
long-term success data frame
}
\description{
Simulates long-term success for a given number of generations, patches, and fitness function
}
\details{

}
\examples{


}
