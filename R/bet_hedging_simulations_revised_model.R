

#' Simulates bet hedging for three host strategies
#'
#' @param gens number of generations
#' @param patches number of soil patches
#' @param seed proportion of occupied patches at the start of simulations
#' @param mean_scarcity the mean probability of a food scarce patch
#' @param var_scarcity the variance in the probability of a food scarce patch
#' @param var_spatial the variance in food abundance with continuous food
#' @param cost cost of infected hosts when food is abundant
#' @param fit.fun fitness function
#' @param germ_prob probability that spores establish in a new patch
#' @param animate boolean to save animation
#' @param file file name to save animation
#' @param plot_food_grid boolean to plot grid of food values
#' @param zero_uninfected bool to zero uninfected spore producution in food-poor environments (removes the outliers)
#'
#' @return list of data frames with simulation results for naive, P. agricolaris infected, and P. hayleyella infected hosts
#'
#' @details
#'
#'
#' @examples
#'
#'
#' @export
#'

simulate_bet_hedging2 <- function(gens = 10, patches = 2, seed = 0.5, mean_scarcity = 0.5, var_scarcity = 0.001, var_spatial = NULL, cost = 0, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.5, animate = FALSE, file = NULL, plot_food_grid = FALSE, zero_uninfected = FALSE) {
  # Create fitness model df
  mod.df <- calculate_betas(zero_uninfected = zero_uninfected)
  # Get kp treatment and symbiosis species
  E <- unique(pf.data.merged$Kp) # get the kp treatments
  Type <- unique(pf.data.merged$Type) # get the types
  # create list for data.frames
  df.list <- list()

  # Set up environment matrix (1's mean there is food, 0 no food)
  start.patches <- round(patches*seed) # number of patches to fill at start
  if (is.null(var_spatial)) {
    # Use beta distribution if there is variation in food scarcity over time
    if (var_scarcity > 0) {
      # First calculate beta parameters
      alpha <- (((1-mean_scarcity)/var_scarcity)-(1/mean_scarcity))*(mean_scarcity^2)
      beta <- alpha*(1/mean_scarcity - 1)
      if (alpha < 0 | beta < 0) {
        stop('negative alpha or beta, use a different mean or variance for food scarcity')
      }
      #print(c(alpha, beta))
      probs_per_gen <- rbeta(gens, alpha, beta)
      #hist(probs_per_gen)
    } else {
      probs_per_gen <- rep(mean_scarcity, gens)
    }
    food_in_patches_vector <- unlist(lapply(probs_per_gen,FUN = function(x) {
      return(rbinom(patches,1,(1-x)))
    }))
  } else {
    #
    nutrients_over_time <- as.vector(fbm(var_scarcity, n = gens))[-(gens+1)] + mean_scarcity
    food_in_patches_vector <- unlist(lapply(nutrients_over_time,FUN = function(x) {
      # draw nutrient concentrations around mean value
      nutrients_t <- rnorm(patches, x, var_spatial)
      # convert to amount of food
      food_t <- exp(-0.5*nutrients_t^2)
      #hist(food_t)
      # First calculate beta parameters
      #alpha <- (((1-x)/var_spatial)-(1/x))*(x^2)
      #beta <- alpha*(1/x - 1)
      #if (alpha < 0 | beta < 0) {
      #  stop('negative alpha or beta, use a different mean or variance for food scarcity')
      #}
      #return(rbeta(patches,alpha,beta))
    }))
  }
  env.matrix <- matrix(food_in_patches_vector, nrow = patches, ncol = gens)
  if (plot_food_grid) {
    food.df <- data.frame('Food' = food_in_patches_vector, 'Time' = sort(rep(1:gens, patches)), 'Patch' = rep(1:patches, gens))
    map_cols <- scale_fill_scico(palette = 'bilbao', limits = c(0,1))
    ggplot(food.df, aes(Time,Patch, fill = Food)) + geom_tile() + xlab('Round') + ylab('Patches') + map_cols + theme(legend.position = 'bottom',axis.ticks.y = element_blank(), axis.text.y = element_blank())
    if (is.null(file)) {
      ggsave('bet_hedging.png')
    } else {
      ggsave(file)
    }
  }
  for (t in Type) {
    if(t == 'Naive') {
      t.cost <- 0
    } else {
      t.cost <- cost
    }


    # Dicty matrix (1's mean the patch is occupied, 0 are unoccupied)
    dicty.matrix <- matrix(c(rep(1, start.patches), rep(0, patches-start.patches)), nrow = patches, ncol = 1)
    # get distributions of spores in no food or food environments
    #no.food.spores <- na.omit(pf.data.merged[pf.data.merged$Kp == E[1] & pf.data.merged$Type == t, ]$Spores)
    #food.spores <- na.omit(pf.data.merged[pf.data.merged$Kp == E[2] & pf.data.merged$Type == t, ]$Spores)
    for (j in 1:gens) {
      # get number of surviving dicty metapopulations
      dicty.count <- length(which(dicty.matrix[, j] > 0))


      # randomly sample entries from model df
      type.sample <- mod.df[mod.df$Type == t,]

      spore.production.sample <- type.sample[sample(1:nrow(type.sample), dicty.count, replace = TRUE),]

      location.in.space <- which(dicty.matrix[, j] == 1)
      spore.production <- spore.production.sample$Intercept+spore.production.sample$B*env.matrix[location.in.space, j]*(1-t.cost)
      dicty.matrix[location.in.space, j] <- spore.production
      # Update dicty matrix with spore counts from previous round
      #dicty.matrix[which(dicty.matrix[, j] > 0 & env.matrix[, j] == 1), j] <- food.sample
      #dicty.matrix[which(dicty.matrix[, j] > 0 & env.matrix[, j] == 0), j] <- no.food.sample
      # Reproduce (each patch seeds new patches proportional to its spore production)
      sori <- round(fit.fun(spore.production))
      sori[!is.finite(sori)] <- 0
      total.sori.for.dispersal <- rbinom(1,sum(sori),germ_prob)
      surviving.offspring.patches <- c()
      # Fill patches until patches are full or there are no more sori
      while (length(surviving.offspring.patches) < patches & total.sori.for.dispersal > 0) {
        offspring.patch <- sample(1:patches, 1)
        total.sori.for.dispersal <- total.sori.for.dispersal - 1
        surviving.offspring.patches <- c(surviving.offspring.patches, offspring.patch)
        surviving.offspring.patches <- unique(surviving.offspring.patches)
      }

      # calculate patches in the next generation and add to matrix
      #patches.in.next.gen <- rep(0, patches)
      #patches.in.next.gen[which(1:patches %in% surviving.offspring.patches)] <- 1
      #increments <- seq(0,total.sori.for.dispersal,by = 10*patches)
      #if (!total.sori.for.dispersal %in% increments) {
      #  increments <- c(increments,total.sori.for.dispersal)
      #}
      #b <- 2
      #while (length(patches.in.next.gen[patches.in.next.gen > 0]) < patches & sum(patches.in.next.gen) < total.sori.for.dispersal) {
      #  print(germ_prob)
      #  print(rmultinom(increments[b]-increments[b-1],1,prob = rep(0.5,patches)))
      #  patches.in.next.gen <- rowSums(rmultinom(increments[b]-increments[b-1],1,prob = rep(0.5,patches)))
        #print(patches.in.next.gen)
      #  total.sori.for.dispersal <- total.sori.for.dispersal-(increments[b]-increments[b-1])
      #  print(c(total.sori.for.dispersal, sum(patches.in.next.gen)))
      #  b <- b+1
      #}
      # calculate patches in the next generation and add to matrix
      patches.in.next.gen <- rep(0, patches)
      patches.in.next.gen[which(1:patches %in% surviving.offspring.patches)] <- 1
      dicty.matrix <- cbind(dicty.matrix, as.vector(patches.in.next.gen))
    }
    # Create output df
    dicty.df <- as.data.frame(as.table(dicty.matrix[, -ncol(dicty.matrix)]))
    dicty.df$Var1 <- as.numeric(dicty.df$Var1)
    if (patches > 1) {
      dicty.df$Var2 <- as.numeric(dicty.df$Var2)
    } else {
      dicty.df <- cbind(rep(1, nrow(dicty.df)),dicty.df)
    }
    names(dicty.df)[1:3] <- c('Patch','Time','Spores')
    dicty.df$Symbiosis <- t
    # Add to list
    df.list[[t]] <- dicty.df
  }
  # Animate generations
  if (animate) {
    d1 <- df.list[["Naive"]]
    d2 <- df.list[["P. agricolaris"]]
    d3 <- df.list[["P. hayleyella"]]
    d.t <- rbind(d1, d2, d3)
    d.t$Symbiosis[d.t$Symbiosis == 'Naive'] <- 'No symbiosis'
    map_cols <- scale_fill_scico(palette = 'bilbao', limits = c(0,max(c(d1$Spores, d2$Freq, d3$Spores))/(10^8)))
    a <- ggplot(d.t, aes(Time, Patch, fill = Spores/(10^8))) + geom_tile() + facet_grid(.~Symbiosis) +
      transition_manual(Time, cumulative = TRUE) + xlab('Round') + ylab('Patches') +
      map_cols + theme(legend.position = 'bottom',axis.ticks.y = element_blank(), axis.text.y = element_blank()) + labs(fill = 'Spores (x10^8)')
    df.list[['ani']] <- animate(a, fps = gens, end_pause = 75, height = 3, width = 5, units = 'in', res = 300)
    #df.list[['ani']] <- animate(a, fps = gens/5, height = 3, width = 5, units = 'in', res = 300)
    if (is.null(file)) {
      anim_save('bet_hedging.gif')
    } else {
      anim_save(file)
    }
  }
  rownames(env.matrix) <- 1:patches
  colnames(env.matrix) <- 1:gens
  df.list[['Food']] <- env.matrix
  return(df.list)
}


#' Simulates long-term success for a given number of generations, patches, and fitness function
#'
#' @param gens number of generations
#' @param patches number of soil patches
#' @param seed proportion of occupied patches at the start of simulations
#' @param fit.fun fitness function
#' @param germ_prob probability that spores are eliminated (from not germinating, making it to fruiting, ect)
#' @param reps number of replicates to simulate
#' @param costs vector of costs to simulate
#' @param mean_scarcity the mean probability of a food scarce patch
#' @param var_scarcities vector of variancesto simulate
#' @param var_spatial vector of spatial variation for continuos food environments
#' @param zero_uninfected bool to zero uninfected spore producution in food-poor environments (removes the outliers)
#'
#' @return long-term success data frame
#'
#' @details
#'
#'
#' @examples
#'
#'
#' @export
#'

simulate_longterm_success2 <- function(gens = 100, patches = 2, seed = 0.25, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.5, reps = 1, costs = seq(0,1, 0.1), mean_scarcity = seq(0.1,0.9,0.1), var_scarcities = seq(0,0.2, 0.1), var_spatial = NULL, zero_uninfected = FALSE) {
  if (is.null(var_spatial)) {
    rows <- reps*length(costs)*length(var_scarcities)*length(mean_scarcity)*3
  } else {
    rows <- reps*length(costs)*length(var_scarcities)*length(mean_scarcity)*length(var_spatial)*3
  }
  out.df <- as.data.frame(matrix(nrow = rows, ncol = 7))
  names(out.df) <- c('Spores','Type','Cost','Mean','Var','Spatial','Food')
  i <- 1
  for (r in 1:reps) {
    print(r)
    for (cst in costs) {
      for (p in var_scarcities) {
        for (m in mean_scarcity) {
          if (is.null(var_spatial)) {
            outlist <- simulate_bet_hedging2(gens = gens, patches = patches, seed = seed, mean_scarcity = m, var_scarcity = p, cost = cst, fit.fun = fit.fun, germ_prob = germ_prob, zero_uninfected = zero_uninfected)
            out.df$Food[i:(i+2)] <- mean(outlist[['Food']])
            outlist[['Food']] <- NULL
            out.df$Spores[i:(i+2)] <- unlist(lapply(outlist, FUN = function(x){return(sum(x[x$Time == max(x$Time),]$Spores))}))
            out.df$Type[i:(i+2)] <- names(outlist)
            out.df$Cost[i:(i+2)] <- cst
            out.df$Mean[i:(i+2)] <- m
            out.df$Var[i:(i+2)] <- p
            out.df$Spatial[i:(i+2)] <- gens*p*(p-1)

            generation.totals <- lapply(outlist, FUN = function(x){return(aggregate(Spores~Time, sum, data = x))})
            out.df$Gmean[i:(i+2)] <- unlist(lapply(generation.totals, FUN = function(x){
              return((exp(mean(log(x$Spores)))))
            }))
            out.df$Amean[i:(i+2)] <- unlist(lapply(generation.totals, FUN = function(x){
              amean <- mean(x$Spores[x$Spores > 0])
              if (is.finite(amean)){
                return(amean)
              } else {
                return(0)
              }
            }))
            out.df$CV[i:(i+2)] <- unlist(lapply(generation.totals, FUN = function(x){
              CV <- sd(x$Spores[x$Spores > 0])/mean(x$Spores[x$Spores > 0])
              if (is.finite(CV)){
                return(CV)
              } else {
                return(10000000)
              }
            }))
            #print(out.df[i:(i+2),])
            out.df$Spore.win[i:(i+2)] <- c(if (out.df$Spores[i]>out.df$Spores[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Spores[i+2]>out.df$Spores[i+1]){TRUE} else {FALSE})
            out.df$Gmean.win[i:(i+2)] <- c(if (out.df$Gmean[i]>out.df$Gmean[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Gmean[i+2]>out.df$Gmean[i+1]){TRUE} else {FALSE})
            out.df$Amean.win[i:(i+2)] <- c(if (out.df$Amean[i]>out.df$Amean[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Amean[i+2]>out.df$Amean[i+1]){TRUE} else {FALSE})
            out.df$Extinct[i:(i+2)] <- out.df$Gmean[i:(i+2)] == 0
            out.df$Extinct.win[i:(i+2)] <- c(if (!out.df$Extinct[i] & out.df$Extinct[i+1]) {TRUE} else {FALSE},FALSE,if(!out.df$Extinct[i+2] & out.df$Extinct[i+1]){TRUE} else {FALSE})
            out.df$TE[i:(i+2)] <- c(if (out.df$Extinct[i] & out.df$Extinct[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Extinct[i+2] & out.df$Extinct[i+1]){TRUE} else {FALSE})
            out.df$CV.win[i:(i+2)] <- c(if (out.df$CV[i]<out.df$CV[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$CV[i+2]<out.df$CV[i+1]){TRUE} else {FALSE})
            i <- i+3
          } else {
            for (v in var_spatial) {
              outlist <- simulate_bet_hedging2(gens = gens, patches = patches, seed = seed, mean_scarcity = m, var_scarcity = p, cost = cst, fit.fun = fit.fun, germ_prob = germ_prob, var_spatial = v)
              out.df$Food[i:(i+2)] <- mean(outlist[['Food']])
              outlist[['Food']] <- NULL
              out.df$Spores[i:(i+2)] <- unlist(lapply(outlist, FUN = function(x){return(sum(x[x$Time == max(x$Time),]$Spores))}))
              out.df$Type[i:(i+2)] <- names(outlist)
              out.df$Cost[i:(i+2)] <- cst
              out.df$Mean[i:(i+2)] <- m
              out.df$Var[i:(i+2)] <- p
              out.df$Spatial[i:(i+2)] <- v
              generation.totals <- lapply(outlist, FUN = function(x){return(aggregate(Spores~Time, sum, data = x))})
              out.df$Gmean[i:(i+2)] <- unlist(lapply(generation.totals, FUN = function(x){
                return((exp(mean(log(x$Spores)))))
              }))
              out.df$Amean[i:(i+2)] <- unlist(lapply(generation.totals, FUN = function(x){
                amean <- mean(x$Spores[x$Spores > 0])
                if (is.finite(amean)){
                  return(amean)
                } else {
                  return(0)
                }
              }))
              out.df$CV[i:(i+2)] <- unlist(lapply(generation.totals, FUN = function(x){
                amean <- mean(x$Spores[x$Spores > 0])
                if (is.finite(amean)){
                  return(sd(x$Spores[x$Spores > 0])/amean)
                } else {
                  return(10000000)
                }
              }))
              out.df$Spore.win[i:(i+2)] <- c(if (out.df$Spores[i]>out.df$Spores[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Spores[i+2]>out.df$Spores[i+1]){TRUE} else {FALSE})
              out.df$Gmean.win[i:(i+2)] <- c(if (out.df$Gmean[i]>out.df$Gmean[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Gmean[i+2]>out.df$Gmean[i+1]){TRUE} else {FALSE})
              out.df$Amean.win[i:(i+2)] <- c(if (out.df$Amean[i]>out.df$Amean[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Amean[i+2]>out.df$Amean[i+1]){TRUE} else {FALSE})
              out.df$Extinct[i:(i+2)] <- out.df$Gmean[i:(i+2)] == 0
              out.df$Extinct.win[i:(i+2)] <- c(if (!out.df$Extinct[i] & out.df$Extinct[i+1]) {TRUE} else {FALSE},FALSE,if(!out.df$Extinct[i+2] & out.df$Extinct[i+1]){TRUE} else {FALSE})
              out.df$TE[i:(i+2)] <- c(if (out.df$Extinct[i] & out.df$Extinct[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$Extinct[i+2] & out.df$Extinct[i+1]){TRUE} else {FALSE})
              out.df$CV.win[i:(i+2)] <- c(if (out.df$CV[i] < out.df$CV[i+1]) {TRUE} else {FALSE},FALSE,if(out.df$CV[i+2] < out.df$CV[i+1]){TRUE} else {FALSE})
              i <- i+3
            }
          }
        }
      }
    }
  }
  return(out.df)
}


#' Uses linear models and contrasts to determine dominant host
#'
#' @param sim.df simulation data
#'
#' @return long-term success data frame
#'
#' @details
#'
#'
#' @examples
#'
#'
#' @export
#'

determine_winner2 <- function(sim.df, y = 'Spores', adjust = FALSE) {
  strategy.df <- as.data.frame(matrix(nrow = length(unique(sim.df$Prob))*length(unique(sim.df$Cost)), ncol = 6))
  names(strategy.df) <- c('Prob','Cost','Type', 'Magnitude','p-value','Fitness')
  index <- 1
  # Loop through simulated data by chance (p) and cost (c)
  for (p in unique(sim.df$Prob)) {
    for (cst in unique(sim.df$Cost)) {
          # Subset data and fit linear models for geometric and arithmetic mean fitness
          df <- sim.df[sim.df$Prob == p & sim.df$Cost == cst, ]
          mod <- lm(get(y)~Type, data = df)
          # Get contrast data
          em <- suppressMessages(emmeans(mod, pairwise~Type, adjust = 'none'))
          # Subset out the means
          em.means.df <- as.data.frame(em$emmeans)
          em.means.df$Type <- factor(em.means.df$Type, ordered = FALSE)
          # Order according to estimated mean
          em.means.df <- em.means.df[order(em.means.df$emmean, decreasing = TRUE), ]
          # Subset out the constrasts
          em.c.df <- as.data.frame(em$contrasts)
          # Check first contrast
          constrasts <- c(paste(em.means.df$Type[2], em.means.df$Type[1], sep = ' - '), paste(em.means.df$Type[1], em.means.df$Type[2], sep = ' - '))
          em.c.df <- em.c.df[which(em.c.df$contrast %in% constrasts), ]
          # Determine where there is a clear best or check other comparisons
          if (em.c.df$p.value > 0.05 & is.finite(em.c.df$p.value)) {
            em.c.df <- as.data.frame(em$contrasts)
            em.c.df <- em.c.df[grep('Naive', em.c.df$contrast), ]
            if (max(em.c.df$p.value) <= 0.05) {
              em.c.df <- em.c.df[em.c.df$p.value == max(em.c.df$p.value), ]
            }
            # output data where both symbionts perform better than naive but not different
            strategy.df[index, ] <- c(p,cst,'Either Symbiont', em.c.df$estimate[1], em.c.df$p.value[1], y)
            index <- index + 1
          } else {
            # Output data with clear best strategy
            strategy.df[index, ] <- c(p,cst,as.character(em.means.df$Type[1]), em.c.df$estimate[1], em.c.df$p.value[1], y)
            index <- index + 1
          }
    }
  }
  if (adjust) {
    # Adjust p values and make non-significant comparisons no best strategy
    strategy.df$`adjusted p-value` <- NA
    strategy.df$`adjusted p-value` <- p.adjust(strategy.df$`p-value`, method = 'fdr')
    #strategy.df$`adjusted p-value`[strategy.df$Fitness == 'Fitness'] <- p.adjust(strategy.df$`p-value`[strategy.df$Fitness == 'Fitness'], method = 'BH')
    strategy.df$Type[strategy.df$`adjusted p-value` > 0.05] <- 'No Best Phenotype'
    strategy.df$Type[strategy.df$Type == 'Naive'] <- 'Uninfected'
  }
  return(strategy.df)
}


#' Uses linear models and contrasts to determine dominant host
#'
#' @param win.df data with winning strategies
#' @param sim.df simulation data
#' @param patches number of patches
#'
#' @return win.df with BH column
#'
#' @details
#'
#'
#' @examples
#'
#'
#' @export
#'

determine_BH2 <- function(win.df, sim.df, patches) {
  win.df$BH <- unlist(apply(win.df, 1, FUN = function(x){
    strategy <- x[3]
    cst <- x[2]
    p <- x[1]
    d <- sim.df[sim.df$Type == strategy & sim.df$Prob == p & sim.df$Cost == cst, ]
    #print(nrow(d))
    if (nrow(d) > 0) {
      gmean <- sample(d$Gmean, length(d$Gmean)*1, replace = TRUE)
      arithmean <- sample(d$Mean, length(d$Mean)*1, replace = TRUE)
      t <- t.test(gmean, arithmean)

      if (t$p.value <= 0.05 & mean(gmean) < mean(arithmean)) {
        return('Y')
      } else {
        return('N')
      }
    } else {
      return('N')
    }
  }))
  return(win.df)
}



#' Plots a histogram for a given mean and variance probability
#'
#' @param mean_scarcity mean probability of food-poor
#' @param var_scarcity variance in probabiltt of food-por
#' @param reps number of replicates
#'
#' @return ggplot histogram
#'
#' @details
#'
#'
#' @examples
#'
#'
#' @export
#'

plot_prob_histogram <- function(mean_scarcity = 0.5, var_scarcity = 0.1, reps = 100) {
  alpha <- (((1-mean_scarcity)/var_scarcity)-(1/mean_scarcity))*(mean_scarcity^2)
  beta <- alpha*(1/mean_scarcity - 1)
  print(c(alpha, beta))
  if (alpha < 0 | beta < 0) {
    stop('negative alpha or beta, use a different mean or variance for food scarcity')
  }
  #print(c(alpha, beta))
  probs_per_gen <- rbeta(reps, alpha, beta)
  print(range(probs_per_gen))
  histogram <- ggplot() + geom_histogram(aes(probs_per_gen)) + xlab('Prob(food-poor)')
  return(histogram)
}


#' calculates regression coefficients for each pair of spore values
#'
#' @param zero_uninfected bool to zero uninfected spore producution in food-poor environments (removes the outliers)
#'
#' @return regression coefficient data frame
#'
#' @details
#'
#'
#' @examples
#'
#'
#' @export
#'

calculate_betas <- function(zero_uninfected = FALSE) {
  # Get baseline fitness without food
  out.df <- pf.data.merged[pf.data.merged$Kp == 'N',]
  with.food <- pf.data.merged[pf.data.merged$Kp == 'Y',]
  # Rename and merge
  with.food$Spores.with.food <- with.food$Spores
  out.df <- merge(out.df, with.food[, c('Spores.with.food','Type','Date','Clone')], by = c('Type','Date','Clone'))
  out.df$B <- out.df$Spores.with.food - out.df$Spores
  # remove unnesseary variables, rename, and return results
  out.df <- out.df[, c(-4,-6)]
  names(out.df)[4] <- 'Intercept'
  if (zero_uninfected) {
    out.df$Intercept[out.df$Type == 'Naive'] <- 0
  }
  return(out.df)

}

#' Calculates proportion of simulations where infections are beneficial (either by bet-hedging to not)
#'
#' @param df replicated simulation output
#' @param food_from_sims bool indicating whether to use mean food from simulations as the mean food
#' @param exclude_food bool indicating whether to exclude food values (make them all equal)
#'
#' @return data frame of proportions
#'
#' @details
#'
#'
#' @examples
#'
#'
#' @export
#'

calculate_proportion <- function(df, food_from_sims = FALSE, exclude_food = FALSE) {
  if (food_from_sims) {
    # Change mean to food (subtract from 1 to make it food scarcity)
    df$Mean <- 1-round(df$Food/0.5,1)*.5
    # Change spatial to single value
    df$Spatial <- 1
  }
  if (exclude_food) {
    # Change mean to single value
    df$Mean <- 1
  }
  # Get data for both species
  df.a <- df[df$Type == 'P. agricolaris', ]
  df.h <- df[df$Type == 'P. hayleyella', ]
  # create new cols with true and false for winners and losers
  df.a$BH <- df.a$Gmean.win & !df.a$Amean.win
  df.h$BH <- df.h$Gmean.win & !df.h$Amean.win
  # Get number of replicates
  Ns <- aggregate(Spores~Cost+Mean+Var+Type+Spatial, df, length)
  names(Ns)[6] <- 'N'
  print(unique(Ns$N))
  # Sum true and falses
  out.a1 <- aggregate(Spore.win~Cost+Mean+Var+Type+Spatial, df.a, sum)
  out.a2 <- aggregate(Gmean.win~Cost+Mean+Var+Type+Spatial, df.a, sum)
  out.a3 <- aggregate(BH~Cost+Mean+Var+Type+Spatial, df.a, sum)
  out.a4 <- aggregate(Extinct.win~Cost+Mean+Var+Type+Spatial, df.a, sum)
  out.a5 <- aggregate(TE~Cost+Mean+Var+Type+Spatial, df.a, sum)
  out.a6 <- aggregate(CV.win~Cost+Mean+Var+Type+Spatial, df.a, sum)

  out.h1 <- aggregate(Spore.win~Cost+Mean+Var+Type+Spatial, df.h, sum)
  out.h2 <- aggregate(Gmean.win~Cost+Mean+Var+Type+Spatial, df.h, sum)
  out.h3 <- aggregate(BH~Cost+Mean+Var+Type+Spatial, df.h, sum)
  out.h4 <- aggregate(Extinct.win~Cost+Mean+Var+Type+Spatial, df.h, sum)
  out.h5 <- aggregate(TE~Cost+Mean+Var+Type+Spatial, df.h, sum)
  out.h6 <- aggregate(CV.win~Cost+Mean+Var+Type+Spatial, df.h, sum)

 # Merge data
  out.a <- merge(out.a1, out.a2, by = c('Cost','Mean','Var','Type','Spatial'))
  out.a <- merge(out.a, out.a3, by = c('Cost','Mean','Var','Type','Spatial'))
  out.a <- merge(out.a, out.a4, by = c('Cost','Mean','Var','Type','Spatial'))
  out.a <- merge(out.a, out.a5, by = c('Cost','Mean','Var','Type','Spatial'))
  out.a <- merge(out.a, out.a6, by = c('Cost','Mean','Var','Type','Spatial'))

  out.h <- merge(out.h1, out.h2, by = c('Cost','Mean','Var','Type','Spatial'))
  out.h <- merge(out.h, out.h3, by = c('Cost','Mean','Var','Type','Spatial'))
  out.h <- merge(out.h, out.h4, by = c('Cost','Mean','Var','Type','Spatial'))
  out.h <- merge(out.h, out.h5, by = c('Cost','Mean','Var','Type','Spatial'))
  out.h <- merge(out.h, out.h6, by = c('Cost','Mean','Var','Type','Spatial'))

  out.df <- rbind(out.h, out.a)
  # Rename columns
  names(out.df)[6:7] <- c('Spores','Gmean')
  out.df <- merge(out.df, Ns, by = c('Cost','Mean','Var','Type','Spatial'))
  # divide by N

  out.df[, 6:11] <- out.df[, 6:11]/out.df$N
  # create fill category to color results
  out.df$Fill <- 'Uninfected'
  out.df$Fill[out.df$Gmean > 0.75] <- 'Infected'
  out.df$Fill[out.df$Fill == 'Infected' & out.df$BH > 0.5*out.df$Gmean] <- 'Bet-hedging'
  out.df$Fill[out.df$Gmean <= 0.75 & out.df$Gmean >= 0.25] <- 'Mixed'
  out.df$Fill[out.df$Fill == 'Mixed' & out.df$BH > 0.1] <- 'Mixed (with bet-hedging)'
  out.df$Fill[out.df$Extinct.win > 0.5] <- 'Extinction (uninfected)'
  out.df$Fill[out.df$TE > 0.5] <- 'Extinction (both)'

  out.df$Fill <- factor(out.df$Fill, levels = c('Uninfected','Infected','Bet-hedging','Mixed','Mixed (with bet-hedging)','Extinction (uninfected)','Extinction (both)'), ordered = TRUE)
  return(out.df)

}
