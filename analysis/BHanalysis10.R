require(devtools)
require(ggplot2)
require(RColorBrewer)
require(cowplot)
install_gitlab('treyjscott/farmerBH')
require(farmerBH)
# Set plot theme
theme_set(theme_cowplot())
pal <- brewer.pal(12, 'Set3')
trey_theme2 <- theme(legend.position = 'none', plot.title = element_text(size = 11, face = 'plain'), axis.text = element_text(size = 8), axis.title = element_text(size = 11))


# Uncomment to re-run simulations
#df_1patch <- simulate_longterm_success(gens = 100, patches = 1, seed = 1, reps = 500, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.1, average = TRUE)
#df_10patch <- simulate_longterm_success(gens = 100, patches = 10, seed = 1, reps = 500, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.1, average = TRUE)
#df_100patch <- simulate_longterm_success(gens = 100, patches = 100, seed = 1, reps = 500, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.1, average = TRUE)

#patch.data <- list(df_1patch, df_10patch, df_100patch)
#save(patch.data, file = 'analysis/data/patch10.Rdata')

load('analysis/data/patch10.Rdata')
df_1patch <- patch.data[[1]]
df_10patch <- patch.data[[2]]
df_1000patch <- patch.data[[3]]
# Determine best strategy w/hayleyella
h_df1_win1 <- determine_winner(df_1patch[!df_1patch$Type == 'P. agricolaris', ], y = 'Gmean', adjust = TRUE)
h_df1_win2 <- determine_winner(df_1patch[!df_1patch$Type == 'P. agricolaris', ], y = 'Mean', adjust = TRUE)
h_df10_win1 <- determine_winner(df_10patch[!df_10patch$Type == 'P. agricolaris', ],y = 'Gmean', adjust = TRUE)
h_df10_win2 <- determine_winner(df_10patch[!df_10patch$Type == 'P. agricolaris', ], y = 'Mean', adjust = TRUE)
h_df100_win1 <- determine_winner(df_100patch[!df_100patch$Type == 'P. agricolaris', ],y = 'Gmean', adjust = TRUE)
h_df100_win2 <- determine_winner(df_100patch[!df_100patch$Type == 'P. agricolaris', ], y = 'Mean', adjust = TRUE)
# Determine best strategy w/agricolaris
a_df1_win1 <- determine_winner(df_1patch[!df_1patch$Type == 'P. hayleyella', ], y = 'Gmean', adjust = TRUE)
a_df1_win2 <- determine_winner(df_1patch[!df_1patch$Type == 'P. hayleyella', ], y = 'Mean', adjust = TRUE)
a_df10_win1 <- determine_winner(df_10patch[!df_10patch$Type == 'P. hayleyella', ],y = 'Gmean', adjust = TRUE)
a_df10_win2 <- determine_winner(df_10patch[!df_10patch$Type == 'P. hayleyella', ], y = 'Mean', adjust = TRUE)
a_df100_win1 <- determine_winner(df_100patch[!df_100patch$Type == 'P. hayleyella', ],y = 'Gmean', adjust = TRUE)
a_df100_win2 <- determine_winner(df_100patch[!df_100patch$Type == 'P. hayleyella', ], y = 'Mean', adjust = TRUE)

#wins1000 <- determine_BH(df1000_win1, df_1000patch, 1000)
# Combine data
h_wins1 <- merge(h_df1_win1, h_df1_win2, by = c('Prob','Cost'))
h_wins10 <- merge(h_df10_win1, h_df10_win2, by = c('Prob','Cost'))
h_wins100 <- merge(h_df100_win1, h_df100_win2, by = c('Prob','Cost'))
a_wins1 <- merge(a_df1_win1, a_df1_win2, by = c('Prob','Cost'))
a_wins10 <- merge(a_df10_win1, a_df10_win2, by = c('Prob','Cost'))
a_wins100 <- merge(a_df100_win1, a_df100_win2, by = c('Prob','Cost'))

# Find where long-term trades off with average
h_wins1$BH <- 'N'
h_wins1$BH[h_wins1$Type.y == 'Uninfected' & (h_wins1$Type.x == 'P. hayleyella')] <- 'Y'
h_wins1$Type.x <- factor(h_wins1$Type.x, c('Uninfected','P. agricolaris','P. hayleyella','Either Symbiont','No Best Phenotype'),ordered = TRUE)
h_wins1$TypeBH <- paste(h_wins1$BH, h_wins1$Type.x, sep = '_')
a_wins1$BH <- 'N'
a_wins1$BH[a_wins1$Type.y == 'Uninfected' & (a_wins1$Type.x == 'P. agricolaris')] <- 'Y'
a_wins1$Type.x <- factor(a_wins1$Type.x, c('Uninfected','P. agricolaris','P. hayleyella','Either Symbiont','No Best Phenotype'),ordered = TRUE)
a_wins1$TypeBH <- paste(a_wins1$BH, a_wins1$Type.x, sep = '_')

h_wins10$BH <- 'N'
h_wins10$BH[h_wins10$Type.y == 'Uninfected' & (h_wins10$Type.x == 'P. hayleyella')] <- 'Y'
h_wins10$Type.x <- factor(h_wins10$Type.x, c('Uninfected','P. agricolaris','P. hayleyella','Either Symbiont','No Best Phenotype'),ordered = TRUE)
h_wins10$TypeBH <- paste(h_wins10$BH, h_wins10$Type.x, sep = '_')
a_wins10$BH <- 'N'
a_wins10$BH[a_wins10$Type.y == 'Uninfected' & (a_wins10$Type.x == 'P. agricolaris')] <- 'Y'
a_wins10$Type.x <- factor(a_wins10$Type.x, c('Uninfected','P. agricolaris','P. hayleyella','Either Symbiont','No Best Phenotype'),ordered = TRUE)
a_wins10$TypeBH <- paste(a_wins10$BH, a_wins10$Type.x, sep = '_')

h_wins100$BH <- 'N'
h_wins100$BH[h_wins100$Type.y == 'Uninfected' & (h_wins100$Type.x == 'P. hayleyella')] <- 'Y'
h_wins100$Type.x <- factor(h_wins100$Type.x, c('Uninfected','P. agricolaris','P. hayleyella','Either Symbiont','No Best Phenotype'),ordered = TRUE)
h_wins100$TypeBH <- paste(h_wins100$BH, h_wins100$Type.x, sep = '_')
a_wins100$BH <- 'N'
a_wins100$BH[a_wins100$Type.y == 'Uninfected' & (a_wins100$Type.x == 'P. agricolaris')] <- 'Y'
a_wins100$Type.x <- factor(a_wins100$Type.x, c('Uninfected','P. agricolaris','P. hayleyella','Either Symbiont','No Best Phenotype'),ordered = TRUE)
a_wins100$TypeBH <- paste(a_wins100$BH, a_wins100$Type.x, sep = '_')

# Order factors
h_wins1$TypeBH <- factor(h_wins1$TypeBH, levels = c("N_Uninfected","N_P. hayleyella","Y_P. hayleyella","N_No Best Phenotype"), ordered = TRUE)
h_wins10$TypeBH <- factor(h_wins10$TypeBH, levels = c("N_Uninfected","N_P. hayleyella","Y_P. hayleyella","N_No Best Phenotype"), ordered = TRUE)
h_wins100$TypeBH <- factor(h_wins100$TypeBH, levels = c("N_Uninfected","N_P. hayleyella","Y_P. hayleyella","N_No Best Phenotype"), ordered = TRUE)
a_wins1$TypeBH <- factor(a_wins1$TypeBH, levels = c("N_Uninfected","N_P. agricolaris","Y_P. agricolaris","N_No Best Phenotype"), ordered = TRUE)
a_wins10$TypeBH <- factor(a_wins10$TypeBH, levels = c("N_Uninfected","N_P. agricolaris","Y_P. agricolaris","N_No Best Phenotype"), ordered = TRUE)
a_wins100$TypeBH <- factor(a_wins100$TypeBH, levels = c("N_Uninfected","N_P. agricolaris","Y_P. agricolaris","N_No Best Phenotype"), ordered = TRUE)

leg <- get_legend(ggplot(a_wins1, aes(as.numeric(Prob),as.numeric(Cost), fill = TypeBH)) + geom_tile(color = 'gray') +
                    scale_fill_manual(values = pal[c(8,5,7,9)], name = '        ', labels = c('Uninfected', 'Infected','Infected (Bet-hedging)','No Best Phenotype')) +
                    theme(legend.position = 'top', legend.key.size = unit(0.2, 'cm'), legend.text = element_text(size = 9)))

c <- ggplot(h_wins1, aes(as.numeric(Prob),as.numeric(Cost), fill = TypeBH)) + geom_tile(color = 'gray') +
  scale_fill_manual(values = pal[c(8,5,7,9)]) + #scale_color_manual(values = c('black')) +
  scale_alpha_manual(values = c(0.45,1)) +
  trey_theme2 + xlab('Prob(food-poor)') + ylab('Infection cost') + ggtitle('1 Patch')

d <- ggplot(h_wins10, aes(as.numeric(Prob),as.numeric(Cost), fill = TypeBH)) + geom_tile(color = 'gray') +
  scale_fill_manual(values = pal[c(8,5,7,9)]) + #scale_color_manual(values = c('black')) +
  scale_alpha_manual(values = c(0.45,1)) +
  trey_theme2 + xlab('Prob(food-poor)') + ylab('Infection cost') + ggtitle('10 Patches')

e <- ggplot(h_wins100, aes(as.numeric(Prob),as.numeric(Cost), fill = TypeBH)) + geom_tile(color = 'gray') +
  scale_fill_manual(values = pal[c(8,5,7,9)]) + #scale_color_manual(values = c('black')) +
  scale_alpha_manual(values = c(0.45,1)) +
  trey_theme2 + xlab('Prob(food-poor)') + ylab('Infection cost') + ggtitle('100 Patches')

f <- ggplot(a_wins1, aes(as.numeric(Prob),as.numeric(Cost), fill = TypeBH)) + geom_tile(color = 'gray') +
  scale_fill_manual(values = pal[c(8,5,7,9)]) + #scale_color_manual(values = c('black')) +
  scale_alpha_manual(values = c(0.45,1)) +
  trey_theme2 + xlab('Prob(food-poor)') + ylab('Infection cost') + ggtitle('1 Patch')

g <- ggplot(a_wins10, aes(as.numeric(Prob),as.numeric(Cost), fill = TypeBH)) + geom_tile(color = 'gray') +
  scale_fill_manual(values = pal[c(8,5,7,9)]) + #scale_color_manual(values = c('black')) +
  scale_alpha_manual(values = c(0.45,1)) +
  trey_theme2 + xlab('Prob(food-poor)') + ylab('Infection cost') + ggtitle('10 Patches')

h <- ggplot(a_wins100, aes(as.numeric(Prob),as.numeric(Cost), fill = TypeBH)) + geom_tile(color = 'gray') +
  scale_fill_manual(values = pal[c(8,5,7,9)]) + #scale_color_manual(values = c('black')) +
  scale_alpha_manual(values = c(0.45,1)) +
  trey_theme2 + xlab('Prob(food-poor)') + ylab('Infection cost') + ggtitle('100 Patches')

# Put everything together
g2 <- cowplot::plot_grid(c, d, e, ncol = 3, labels = c('B','',''))
g3 <- cowplot::plot_grid(f, g, h, ncol = 3, labels = c('A','',''))
cowplot::plot_grid(g3, g2, leg, nrow = 3, rel_heights = c(1,1,0.1))
ggsave('Figures/FigureS2.tiff', width = 5.75, height = 4, dpi = 300)
