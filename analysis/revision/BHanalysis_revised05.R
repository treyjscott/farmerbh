require(devtools)
require(ggplot2)
require(RColorBrewer)
require(cowplot)
#install_gitlab('treyjscott/farmerBH')
require(farmerBH)
# Set up parameters
vars <- 10^-seq(1.25, 4, length.out = 20)
means <- seq(0.1,0.9, length.out = 20)
means2 <- seq(0.05,0.95, length.out = 20)

# Binary food
patch.data <- list('c = 0' = c(), 'c = 0.25' = c(), 'c = 0.5' = c(), 'c = 0.75' = c(),
                   'c = 0 v' = c(), 'c = 0.25 v' = c(), 'c = 0.5 v' = c(), 'c = 0.75 v' = c())
# Simulate and save results
for (i in 1:100) {
  print(i)
  df_c0 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = means, var_scarcities = vars, costs = c(0))
  df_c01 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = means, var_scarcities = vars, costs = c(0.25))
  df_c02 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = means, var_scarcities = vars, costs = c(0.5))
  df_c03 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = means, var_scarcities = vars, costs = c(0.75))
  patch.data[['c = 0']] <- rbind(patch.data[['c = 0']],df_c0)
  patch.data[['c = 0.25']] <- rbind(patch.data[['c = 0.25']],df_c01)
  patch.data[['c = 0.5']] <- rbind(patch.data[['c = 0.5']],df_c02)
  patch.data[['c = 0.75']] <- rbind(patch.data[['c = 0.75']],df_c03)
  # Draw random resource value
  resource_mean <- rnorm(1)
  df_c0 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = resource_mean, var_scarcities = means2, var_spatial = means2, costs = c(0))
  df_c01 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = resource_mean, var_scarcities = means2, var_spatial = means2, costs = c(0.25))
  df_c02 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = resource_mean, var_scarcities = means2, var_spatial = means2, costs = c(0.5))
  df_c03 <- simulate_longterm_success2(gens = 100, patches = 100, seed = 1, reps = 1, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.05, mean_scarcity = resource_mean, var_scarcities = means2, var_spatial = means2, costs = c(0.75))
  patch.data[['c = 0 v']] <- rbind(patch.data[['c = 0 v']],df_c0)
  patch.data[['c = 0.25 v']] <- rbind(patch.data[['c = 0.25 v']],df_c01)
  patch.data[['c = 0.5 v']] <- rbind(patch.data[['c = 0.5 v']],df_c02)
  patch.data[['c = 0.75 v']] <- rbind(patch.data[['c = 0.75 v']],df_c03)
  save(patch.data, file = 'analysis/data/patch_revision05_2.Rdata')
}


#patch.data[['c = 0 v']] <- c()
#patch.data[['c = 0.25 v']] <- c()
#patch.data[['c = 0.5 v']] <- c()
#patch.data[['c = 0.75 v']] <- c()

#patch.data <- list(df_p010, df_p025, df_p050, df_p075, df_p090)



#h_df1_win <- determine_winner(df_c0[!df_c0$Type == 'P. agricolaris', ], y = 'Spores', adjust = TRUE)
out_df0 <- calculate_proportion(patch.data[[1]])
out_df25 <- calculate_proportion(patch.data[[2]])
out_df50 <- calculate_proportion(patch.data[[3]])
out_df75 <- calculate_proportion(patch.data[[4]])

ggplot(patch.data[[1]][patch.data[[1]]$Type == 'P. agricolaris', ], aes(log(Var),Mean, fill = Gmean.win)) + geom_tile()


a <- ggplot(out_df0[out_df0$Type == 'P. agricolaris',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df0[out_df0$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0')
e <- ggplot(out_df0[out_df0$Type == 'P. hayleyella',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df0[out_df0$Type == 'P. hayleyella',]$Fill))])

b <- ggplot(out_df25[out_df25$Type == 'P. agricolaris',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df25[out_df25$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0.25')
f <- ggplot(out_df25[out_df25$Type == 'P. hayleyella',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df25[out_df25$Type == 'P. hayleyella',]$Fill))])

c <- ggplot(out_df50[out_df50$Type == 'P. agricolaris',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df50[out_df50$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0.5')
g <- ggplot(out_df50[out_df50$Type == 'P. hayleyella',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df50[out_df50$Type == 'P. hayleyella',]$Fill))])

d <- ggplot(out_df75[out_df75$Type == 'P. agricolaris',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df75[out_df75$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0.75')
h <- ggplot(out_df75[out_df75$Type == 'P. hayleyella',], aes(log(Var),Mean, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df75[out_df75$Type == 'P. hayleyella',]$Fill))])

plot_grid(a,b,c,d,e,f,g,h, nrow = 2, ncol = 4, align = 'vh')

out_df0v <- calculate_proportion(patch.data[[5]])
out_df25v <- calculate_proportion(patch.data[[6]])
out_df50v <- calculate_proportion(patch.data[[7]])
out_df75v <- calculate_proportion(patch.data[[8]])

a2 <- ggplot(out_df0v[out_df0v$Type == 'P. agricolaris',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df0v[out_df0v$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0')
e2 <- ggplot(out_df0v[out_df0v$Type == 'P. hayleyella',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df0v[out_df0v$Type == 'P. hayleyella',]$Fill))])

b2 <- ggplot(out_df25v[out_df25v$Type == 'P. agricolaris',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df25v[out_df25v$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0.25')
f2 <- ggplot(out_df25v[out_df25v$Type == 'P. hayleyella',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df25v[out_df25v$Type == 'P. hayleyella',]$Fill))])

c2 <- ggplot(out_df50v[out_df50v$Type == 'P. agricolaris',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df50v[out_df50v$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0.5')
g2 <- ggplot(out_df50v[out_df50v$Type == 'P. hayleyella',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df50v[out_df50v$Type == 'P. hayleyella',]$Fill))])

d2 <- ggplot(out_df75v[out_df75v$Type == 'P. agricolaris',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df75v[out_df75v$Type == 'P. agricolaris',]$Fill))]) +
  ggtitle('c = 0.75')
h2 <- ggplot(out_df75v[out_df75v$Type == 'P. hayleyella',], aes(Var,Spatial, alpha = BH, fill = Fill)) + geom_tile() + scale_alpha_continuous(range = c(0.5,1)) + trey_theme2 +
  scale_fill_manual(values = pal[which(fill_index %in% unique(out_df75v[out_df75v$Type == 'P. hayleyella',]$Fill))])

plot_grid(a2,b2,c2,d2,e2,f2,g2,h2, nrow = 2, ncol = 4)


