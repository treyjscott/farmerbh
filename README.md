
# Description 

This directory contains data, code, and the bet-hedging simulation program from Scott et al. (2021) 

# Directory layout

Code for repeating the analysis can be found inside the analysis directory. The files are:
fitness_analysis.R - code for making figures using empirical data in data/
BHanalysisX.R - these files have code for running simulations with X = g = 10, 50, and 90. Also contains code to make figures.

Data for statistical analysis is included in the analysis directory (inside the data directory). The files in this directory are:

day_6_data.csv - data with OD and spore measurements after 6 days of growth

extra_day_data.csv - data with 8 and 12 days of growth

predicted.results.csv - data with CFU counts for supplemental figure

Rdata files are results of simulations with g = 10, 50, and 90

Other directories are for the simulation program.

### UPDATE 2/22/2022 ###
A new folder containing code for revised simulations was added to the analysis folder. These follow the BHanalysisX.R conventions, but now are labeled BHanalysis_revisedX.R. The BHanalysis.R file contains code for doing analysis and plotting results.

We ran binary and continuous simulations in two sets (denoted with _2 in file names), but code in BHanalysisX.R files will output a single dataset with both kinds of simulations. This will be important if you try to run your own simulations and plot them using the code in BHanalysis.R.  



# Installing the simulation program

The bet-hedging simulation program farmerBH can be installed in R using devtools.

```
# Install devtools
install.packages("devtools")

# Install SimGM
install_gitlab("treyjscott/farmerBH")
```

# Bet-hedging simulations

This figure details how simulations are performed (also see methods in paper).

![](Figures/bet_hedging_sims/Slide1.jpg){width=25%} 

Bet-hedging simulations are run with the simulate_bet_hedging function and replicated with the simulate_longterm_success function.
```
bh.data <- simulate_bet_hedging(gens = 100, patches = 1, seed = 1)
df_1patch <- simulate_longterm_success(gens = 100, patches = 1, seed = 1, reps = 500, fit.fun = function(x){return(x/(2*10^5))}, germ_prob = 0.5, average = TRUE)
```
Simulation code for the paper can be found in the BHanalysis files inside the analysis directory.
